#range generates a list of elements smaller then a given value
a=range(5)
print a
#adding elements
a.append(5)
a.append("six")
a.append([1,2])
print a
#retrieving an element
print a[-1]
#removing elements
del a[-1]
a.remove("six")
a.pop(5)
print a

#tuples
# wd=('do','not','modify')
# print wd[0]
# wd.append('test')
# wd.remove('do')

#slicing
b=a[:]
print b
b=a[3:]
print b
b=a[1:3]
print b
b=a[::-1]
print b
