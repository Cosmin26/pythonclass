import pprint
#create a chess board
chess_board = [[0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0],
               [0,0,0,0,0,0,0,0]]
pprint.pprint(chess_board)
#create a chess piece
chess_piece = {"name":"Bishop",
               "colour":"black",
               "position":(3,3)}
#place chess piece on board
piece_pos = chess_piece["position"]
chess_board[piece_pos[0]][piece_pos[1]] = chess_piece["name"]
pprint.pprint(chess_board)